#include "inc/headers.h"
#include "inc/print.h"
#include "inc/log.h"
#include "inc/time.h"
#include "inc/average.h"

#include "inc/cube.h"

/*   Glut functions   */
void reshape(int width, int height);
void display();
void displayMainMenu();
void displayTimingScreen();
void keyFunc(unsigned char key, int x, int y);
void idle();
/* ------------------ */

/*   Misc functions   */
std::string getScramble();
void drawGuide();
/* ------------------ */

    // 4 cubes for each corner
    // not sure if really needed....
    static Cube blCube(-1.1,-1.1,3);
    static Cube brCube(1.1,-1.1,3);
    static Cube tlCube(-1.1,1.1,3);
    static Cube trCube(1.1,1.1,3);

    // states for the window
    static const int MAINMENU = 0;
    static const int TIMINGSCREEN3 = 3;
    static const int TIMINGSCREEN4 = 4;
    static const int TIMINGSCREEN5 = 5;
    static int windowState = MAINMENU;

    // 3 average classes for each cube size
    Average average3(3);
    Average average4(4);
    Average average5(5);
    Average* currentAverage;

/* --- Main --- */
int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(550,550);
    glutInitWindowPosition(0,0);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Cube Timer v1.0 by Matt Traudt");
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyFunc);
    glutIdleFunc(idle);
//
    srand(time(0));
//
    glutMainLoop();
    return 0;
}
/* --- ---- --- */



void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(-5,5,-5,5,5,-5);
    //glFrustum(-5,5,-5,5, 0,100000);
    gluPerspective(45,width/height,0.01,10);

    glClearColor(.25,.25,.25,1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    gluLookAt(0,0,5, 0,0,0, 0,1,0);
    glPushMatrix();

    //drawGuide();

    if (windowState == MAINMENU)
        {displayMainMenu();}
    else if (windowState == TIMINGSCREEN3 ||
             windowState == TIMINGSCREEN4 ||
             windowState == TIMINGSCREEN5  )
        {displayTimingScreen();}


    glPopMatrix();

    glutSwapBuffers();
}

void displayMainMenu()
{
    glPushMatrix();
        tlCube.setType(3);
        blCube.setType(4);
        brCube.setType(5);
        tlCube.draw();
        blCube.draw();
        brCube.draw();
    glPopMatrix();
    glPushMatrix();
        using namespace Print;
        // translate so words are
        // in front of potential cubes
        glTranslatef(0,0,.6);
        setFont(lgFont);
        glColor3f(0,1,1);
        setRasterPos(0.25,1.35);
        printString("Matt's Cube Timer");
        setFont(smFont);
        printString("\n\nBatteries not included,\nbut please enjoy these\nspinning cubes.");
        setFont(lgFont);
        printString("\n\n\nSelect cube size.");
        setFont(smFont); printString("\n");
        setFont(smFont);
        printString("\n(3)   3 x 3\n(4)   4 x 4\n(5)   5 x 5");
    glPopMatrix();


}
void displayTimingScreen()
{
    Time::updateTime();

    glPushMatrix();
        blCube.draw();
    glPopMatrix();
    glPushMatrix();
        using namespace Print;
        glColor3f(0,1,1);

        // Average display
        setRasterPos(-1.5,1.25);
        setFont(lgFont);
        printString("Averages\n");

        for (unsigned int i = 5; i <= 20; i += 5)
        {
            if (i == 5) printString(" ");
            printNum(i); printString(": ");
            int avgTime = currentAverage->getAverage(i);
            if (avgTime > 0) printTime(avgTime);
            else printString("---");

            printString("\n");
        }

        // Timer display
        setRasterPos(.5,.5);
        setFont(smFont);
        printString("Previous time\n");
        printTime(currentAverage->getLastTime());
        setFont(lgFont);
        printString("\n\nCurrent time\n");
        printTime();

        // Help display
        setRasterPos(-.3,-1.4);
        setFont(smFont);
        printString("(s) start timer\n(p) pause timer\n(s, p) restart paused timer\n(r) reset timer\n(spacebar) stop and record time\n(w) generate scramble\n(e) start evaluation timer, then timer\n(q) quit to main menu\n");

        //scramble display
        setRasterPos(-1.5,1.75);
        setFont(smFont);
        printScramble();
        // countdown display
        if (Time::getState() == 3) // countdown
        {
            setRasterPos(0, 1.5);
            setFont(lgFont);
            printString("Examination time.\n");
            printTime(Time::getCountdownTime());
        }

    glPopMatrix();
}

void keyFunc(unsigned char key, int x, int y)
{
    if (windowState == MAINMENU)
    {
        switch (key)
        {
            case '3': windowState = TIMINGSCREEN3;
                      currentAverage = &average3;
                      blCube.setType(3);
                      break;
            case '4': windowState = TIMINGSCREEN4;
                      currentAverage = &average4;
                      blCube.setType(4);
                      break;
            case '5': windowState = TIMINGSCREEN5;
                      currentAverage = &average5;
                      blCube.setType(5);
                      break;

            case 'q': exit(0); break;
        }
    }
    else if (windowState == TIMINGSCREEN3 ||
             windowState == TIMINGSCREEN4 ||
             windowState == TIMINGSCREEN5  )
    {
        switch (key)
        {
            case 'q': Time::resetTime();
                      Print::deleteScramble();
                      windowState = MAINMENU; break;

            case 's': Time::startTime(); break;
            case 'r': Time::resetTime(); break;
            case 'p': Time::pauseTime(); break;
            case ' ': Time::stopTime(currentAverage); break;
            case 'e': Time::startCountdown(); break;
            case 'w': if (windowState==TIMINGSCREEN3)
                      {
                          Time::resetCountdown();
                          Print::needScramble(); break;
                      }
        }
    }

    glutPostRedisplay();
}

void idle()
{
    glutPostRedisplay();
}

void drawGuide()
{
    glBegin(GL_LINES);
    glColor3f(1,0,0);
        glVertex3f(0,0,0);
        glVertex3f(0,1,0);
    glColor3f(0,0,1);
        glVertex3f(0,0,0);
        glVertex3f(1,0,0);
    glColor3f(0,1,0);
        glVertex3f(0,0,0);
        glVertex3f(0,0,1);
    glEnd();
    glBegin(GL_POINTS);
    glColor3f(1,1,1);
        for (int i = -10; i <= 10; i++)
        {
            for (int j = -10; j <= 10; j++)
            {
                glVertex3f(i,j,0);
            }
        }
    glEnd();
}



