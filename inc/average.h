#ifndef AVERAGE_H_INCLUDED
#define AVERAGE_H_INCLUDED
#include "headers.h"

class Average
{
    public:
        bool changed;
        std::string getFileName() {return file;}

        int getLastTime() {return lastTime;}
        int getAverage(unsigned int numEntries);

        Average(int cubeType);
        ~Average();
    protected:
    private:
        int lastTime;
        int average5;
        int average10;
        int average15;
        int average20;

        std::string file;
};

/*
namespace Average
{
    static bool changed;


    int average(unsigned int numEntries);
};

*/
#endif // AVERAGE_H_INCLUDED


