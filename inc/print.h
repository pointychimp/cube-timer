#ifndef PRINT_H_INCLUDED
#define PRINT_H_INCLUDED

#include "headers.h"

namespace Print
{
    //static void* lgFont = GLUT_BITMAP_HELVETICA_18;
    //static void* medFont = GLUT_BITMAP_HELVETICA_12;
    //static void* smFont = GLUT_BITMAP_HELVETICA_10;
    static void* lgFont = GLUT_BITMAP_9_BY_15;
    static void* smFont = GLUT_BITMAP_8_BY_13;

    void setFont(void* f);
    void setRasterPos(float x, float y);

    void printNum(int num);
    void printDigit(int dig);
    // first overload
    // prints current time
    void printTime();
    // second overload
    // prints time from
    // given milliseconds
    void printTime(int milliseconds);
    void printString(std::string str);
    void newLine();

    void printScramble();
    void needScramble();
    void deleteScramble();
};



#endif // PRINT_H_INCLUDED
