#ifndef TIME_H_INCLUDED
#define TIME_H_INCLUDED
#include "headers.h"

namespace Time
{
    void startCountdown();
    void updateCountdown();
    int getCountdownTime();
    void resetCountdown();

    void startTime();
    void stopTime(void* currentAveragePtr);
    void pauseTime();
    void resetTime();
    void updateTime();

    int getState();

    /* returns total time recorded
     * in milliseconds */
    unsigned int getTotalTime();
    /* updates ptr values with appropriate time data
     * from given number of millisceconds */
    void getTotalTime (unsigned int givenMS, int* d, int* h, int* m, int* s, int* ms);

    /*returns CURRENT time data */
    unsigned int getDays();
    unsigned int getHours();
    unsigned int getMinutes();
    unsigned int getSeconds();
    unsigned int getMilliseconds();

};
#endif // TIME_H_INCLUDED
