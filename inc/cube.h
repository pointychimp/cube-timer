#ifndef CUBE_H
#define CUBE_H
#include "headers.h"

class Cube
{
    public:
        void draw();
        int getType() {return type;}
        void setType(int i);
        float getX() {return posX;}
        float getY() {return posY;}

        Cube(float x, float y, float t=3);
        virtual ~Cube();
    protected:
    private:
        float rotation; // angle of rotation
        float rot; // rotation speed
        int type;

        float gap;
        float s;

        float posX, posY;

        void drawSide(float data[15]);
};

#endif // CUBE_H
