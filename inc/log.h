#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED
#include <fstream>
#include <time.h>
#include "headers.h"

namespace Log
{
    void write(std::string fileName, unsigned int time);
    unsigned int readInt();
    std::string readString();

    void openLog(std::string fileName);
    bool logIsOpen();
    bool logIsEOF();
    void closeLog();

    // only to be used interally
    void getDateTime(char* t);
};


#endif // LOG_H_INCLUDED
