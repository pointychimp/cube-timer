#include "../inc/time.h"
#include "../inc/log.h"
#include "../inc/print.h"
#include "../inc/average.h"


    /* used for converting large number
     * of milliseconds to appropriate
     * unit */
    const unsigned int MSinDAY = 86400000;
    const unsigned int MSinHOUR = 3600000;
    const unsigned int MSinMINUTE = 60000;
    const unsigned int MSinSECOND = 1000;

    /* used for calculating how
     * many milliseconds have passed
     * in current second */
    static unsigned int first;
    static unsigned int second;

    /* holds passed time data
     * so second-first (milliseconds)
     * is always below 1000 */
    static unsigned int days=0, hours=0,
                        minutes=0, seconds=0;
    /* for use when timer is
     * paused and is started again.
     * hangover is subtracted from
     * starting tickcount */
    static int hangover=0;

    /* for use during examination time */
    static int countdownFirst, countdownSecond;

    /* constants for the timer's
     * state */
    const int STARTED = 1;
    const int STOPPED = 0;
    const int PAUSED = 2;
    const int COUNTDOWN = 3;
    static int state = 0;


void Time::startCountdown()
{
    if (state == STOPPED)
    {
        countdownFirst = GetTickCount();
        countdownSecond = countdownFirst + 10000;
        Print::deleteScramble();
        state = COUNTDOWN;
    }
    else Time::resetCountdown();
}
void Time::updateCountdown()
{
    if (state == COUNTDOWN)
    {
        countdownFirst = GetTickCount();
        if (countdownFirst >= countdownSecond)
        {
            Beep(1000,300);
            state = STARTED;
        }
    }
}
int Time::getCountdownTime()
{
    if (state == COUNTDOWN)
    {
        Time::updateCountdown();
        return countdownSecond - countdownFirst;
    }
    else return 0;
}
void Time::resetCountdown()
{
    countdownFirst = countdownSecond = 0;
    state = STOPPED;
}

/* restarts the timer at current time
 * if it was paused, or starts at beginning
 * if it was stopped */
void Time::startTime()
{
    if (state==PAUSED)
    {
        first = GetTickCount() - hangover;
        hangover = 0;
    }
    else if (state==STOPPED)
    {
        first = GetTickCount();
    }
    state = STARTED;
}
/* stops timer. Sets it state to stopped. */
void Time::stopTime(void* currentAveragePtr)
{
    if (state != STOPPED)
    {
        state = STOPPED;
        Log::write(((Average*)currentAveragePtr)->getFileName(),
                   Time::getTotalTime());
        ((Average*)currentAveragePtr)->changed = true;

        Time::resetTime();
    }
}
/* pauses timer if started, starts timer if paused.
 * uses "hangover" var to keep track of milliseconds */
void Time::pauseTime()
{
    if (state==STARTED)
    {
        state = PAUSED;
        hangover = second-first;
    }
    else if (state==PAUSED)
    {
        Time::startTime();
    }
}
/* sets everything to null values */
void Time::resetTime()
{
    days = hours = minutes = seconds = 0;
    second = first = 0;
    state = STOPPED;
}
/* updates time. To access it, must use
 * the func get<timeUnit> and format as
 * desired */
void Time::updateTime()
{
    if (state==STARTED)
    {
        second = GetTickCount();
        if (second-first >= 1000)
        {
            first = second;
            seconds++;
        }
        if (seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }
        if (minutes >= 60)
        {
            minutes -= 60;
            hours++;
        }
        if (hours >= 24)
        {
            hours -= 24;
            days++;
        }
    }
}

int Time::getState() {return state;}

unsigned int Time::getTotalTime()
{
    unsigned int ms = 0;
    ms += Time::getDays()*MSinDAY;
    ms += Time::getHours()*MSinHOUR;
    ms += Time::getMinutes()*MSinMINUTE;
    ms += Time::getSeconds()*MSinSECOND;
    ms += Time::getMilliseconds();

    return ms;
}
void Time::getTotalTime(unsigned int givenMS, int* d, int* h, int* m, int* s, int* ms)
{
    if (givenMS >= 1000)
    {
        givenMS -= 1000;
        (*s)++;
    }
    if (*s >= 60)
    {
        *s -= 60;
        (*m)++;
    }
    if (*m >= 60)
    {
        *m -= 60;
        (*h)++;
    }
    if (*h >= 24)
    {
        *h -= 24;
        (*d)++;
    }
    if (givenMS >= 1000)
        getTotalTime(givenMS, d,h,m,s,ms);
    else *ms = givenMS;
}

unsigned int Time::getDays()
    {return days;}
unsigned int Time::getHours()
    {return hours;}
unsigned int Time::getMinutes()
    {return minutes;}
unsigned int Time::getSeconds()
    {return seconds;}
unsigned int Time::getMilliseconds()
    {return second-first;}

