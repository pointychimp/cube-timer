#include "../inc/cube.h"

// gap between tiles and side lengths
// 5
static const float gap5 = .0075;
static const float s5 = (1.0 - (6*gap5)) / 5.0;
// 4
static const float gap4 = .01;
static const float s4 = (1.0 - (5*gap4)) / 4.0;
// 3
static const float gap3 = .015;
static const float s3 = (1.0 - (4*gap3)) / 3.0;

void Cube::setType(int i)
{
    int old = type;
    if (i == 3)
        {gap = gap3; s = s3;}
    else if (i == 4)
        {gap = gap4; s = s4;}
    else if (i == 5)
        {gap = gap5; s = s5;}
    type = i;
    if (type != old)
        {rot = ((rand()%20)+10.0)/100.0;}
}

void Cube::draw()
{
    glPushMatrix();

    static float backYellow[] = {-.5,-.5,-.5,-.5,.5,-.5,.5,.5,-.5,.5,-.5,-.5,   .96863,.85098,.09020};
    static float frontWhite[] = {-.5,-.5,.5,-.5,.5,.5,.5,.5,.5,.5,-.5,.5,       1     ,1     ,1     };
    static float rightBlue[]  = {.5,-.5,-.5,.5,.5,-.5,.5,.5,.5,.5,-.5,.5,       0     ,.27843,.71373};
    static float leftGreen[]  = {-.5,-.5,-.5,-.5,.5,-.5,-.5,.5,.5,-.5,-.5,.5,   0     ,.58431,.26275};
    static float topOrange[]  = {-.5,.5,-.5,-.5,.5,.5,.5,.5,.5,.5,.5,-.5,       .93725,.41961,0     };
    static float bottomRed[]  = {-.5,-.5,-.5,-.5,-.5,.5,.5,-.5,.5,.5,-.5,-.5,   .68235,0     ,.33333};

    glTranslatef(getX(),getY(),0);

    rotation += rot;
    if (rotation >= 360) rotation -= 360;
    glRotatef(80,0,1,0);
    glRotatef(rotation,1,0,1);

    drawSide(backYellow);
    drawSide(frontWhite);
    drawSide(rightBlue);
    drawSide(leftGreen);
    drawSide(topOrange);
    drawSide(bottomRed);

    glPopMatrix();
}

void Cube::drawSide(float data[])
{
    // first draw black background
    glColor3f(0,0,0);
    glutSolidCube(.999);

    // then use correct color
    // to draw small squares
    glColor3fv(&data[12]);
    // (x,y,z)1 = (0,1,2) & (x,y,z)3 = (6,7,8)
    if (data[6]-data[0] != 0)
    {
        if (data[7]-data[1] != 0)
        {
            // x and y
            float z = data[2];
            for (float x = data[0] + gap; x+s+gap <= data[6]; x += s+gap)
            {
                for (float y = data[1] + gap; y+s+gap <= data[7]; y += s+gap)
                {
                    glBegin(GL_QUADS);
                        glVertex3f(x,y,z);
                        glVertex3f(x,y+s,z);
                        glVertex3f(x+s,y+s,z);
                        glVertex3f(x+s,y,z);
                    glEnd();
                }
            }
        }
        else
        {
            // x and z
            float y = data[1];
            for (float x = data[0] + gap; x+s+gap <= data[6]; x += s+gap)
            {
                for (float z = data[2] + gap; z+s+gap <= data[8]; z += s+gap)
                {
                    glBegin(GL_QUADS);
                        glVertex3f(x,y,z);
                        glVertex3f(x,y,z+s);
                        glVertex3f(x+s,y,z+s);
                        glVertex3f(x+s,y,z);
                    glEnd();
                }
            }
        }
    }
    else
    {
        // y and z
        float x = data[0];
        for (float y = data[1] + gap; y+s+gap <= data[7]; y += s+gap)
        {
            for (float z = data[2] + gap; z+s+gap <= data[8]; z += s+gap)
            {
                glBegin(GL_QUADS);
                    glVertex3f(x,y,z);
                    glVertex3f(x,y,z+s);
                    glVertex3f(x,y+s,z+s);
                    glVertex3f(x,y+s,z);
                glEnd();
            }
        }
    }
}


Cube::Cube(float x, float y, float t)
{
    rotation = 0;
    rot = ((rand()%20)+10.0)/100.0;
    setType(t);
    posX = x;
    posY = y;
}

Cube::~Cube()
{
    //dtor
}
