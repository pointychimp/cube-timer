#include "../inc/average.h"
#include "../inc/log.h"


int Average::getAverage(unsigned int numEntries)
{
    if (changed)
    {
        unsigned int times[numEntries];
        times[0] = 0;
        int av = 0;
        bool notEnough = false;
        bool error = false;

        Log::openLog(file);
        if (Log::logIsOpen())
        {
            std::string datetime;
            for (unsigned int i = 0; i < numEntries && !notEnough; i++)
            {
                datetime = Log::readString();
                times[i] = Log::readInt();
                av += times[i];
                if (Log::logIsEOF() && i < numEntries) notEnough = true;
            }
            if (!notEnough)
            {
                av /= numEntries;
                Log::closeLog();
            }
        }
        else {std::cout << "Couldn't open log to read averages.\n"; error = true;}

        Log::closeLog();
        /// since it's changed, need
        /// to update all averages recursively!!!!
        if (numEntries < 20) getAverage(numEntries+5);
        ///
        ///
        changed = false;

        if (error)
        {
            if (numEntries == 5) average5 = -1;
            else if (numEntries == 10) average10 = -1;
            else if (numEntries == 15) average15 = -1;
            else average20 = -1;
        }
        if (notEnough && av > 0) lastTime = times[0];
        if (notEnough) return -1;
        else
        {
            lastTime = times[0];
            if (numEntries == 5) average5 = av;
            else if (numEntries == 10) average10 = av;
            else if (numEntries == 15) average15 = av;
            else average20 = av;
            return av;
        }
    }
    else
    {
        if (numEntries == 5) return average5;
        else if (numEntries == 10) return average10;
        else if (numEntries == 15) return average15;
        else return average20;
    }
}


Average::Average(int cubeType)
{
    // ctor
    changed = true;
    if (cubeType == 3)
        file = "3.txt";
        //file = "D:\\Games_And_Programming\\CodeBlocks Builds\\Cube Timer\\3.txt";
    else if (cubeType == 4)
        file = "4.txt";
        //file = "D:\\Games_And_Programming\\CodeBlocks Builds\\Cube Timer\\4.txt";
    else if (cubeType == 5)
        file = "5.txt";
        //file = "D:\\Games_And_Programming\\CodeBlocks Builds\\Cube Timer\\5.txt";

}
Average::~Average()
{
    // dtor
}



