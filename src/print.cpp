#include "../inc/print.h"
#include "../inc/time.h"

    static void* font = Print::lgFont;

    static float xRaster = 0; //state-based, farthest left x-pos should be
                              //(return point when "hitting enter")
    static float yRaster = 0; //state-based, line it's printing on.


    static bool needNewScramble = false;
    static std::string pattern = "";

void Print::setFont(void* f)
{
    font = f;
}
void Print::setRasterPos(float x, float y)
{
    xRaster = x;
    yRaster = y;
    glRasterPos2f(xRaster, yRaster);
}
void Print::printNum(int num)
{
    if (num < 0) {glutBitmapString(font, (const unsigned char*)"-"); num *= -1;}

    int digit;
    bool haveDisplayed = false;
    for (int fac = 1000000000; fac >= 1; fac /= 10)
    {
        digit = num / fac;
        if ((digit != 0 && !haveDisplayed) || (fac == 1 && !haveDisplayed)) haveDisplayed = true;
        if (haveDisplayed) printDigit(digit);
        num -= fac*digit;
    }
}
void Print::printDigit(int dig)
{
    switch (dig)
    {
        case 1: glutBitmapString(font, (const unsigned char*) "1"); break;
        case 2: glutBitmapString(font, (const unsigned char*) "2"); break;
        case 3: glutBitmapString(font, (const unsigned char*) "3"); break;
        case 4: glutBitmapString(font, (const unsigned char*) "4"); break;
        case 5: glutBitmapString(font, (const unsigned char*) "5"); break;
        case 6: glutBitmapString(font, (const unsigned char*) "6"); break;
        case 7: glutBitmapString(font, (const unsigned char*) "7"); break;
        case 8: glutBitmapString(font, (const unsigned char*) "8"); break;
        case 9: glutBitmapString(font, (const unsigned char*) "9"); break;
        case 0: glutBitmapString(font, (const unsigned char*) "0"); break;
    }
}
void Print::printTime()
{
    int d = Time::getDays();
    int h = Time::getHours();
    int m = Time::getMinutes();
    int s = Time::getSeconds();
    int ms = Time::getMilliseconds();

    if (d>0) {printNum(d); printString("d ");}
    if (h>0 || d>0) {printNum(h); printString("h ");}
    if (m>0 || h>0 || d>0) {printNum(m); printString("m ");}
    printNum(s); printString(".");
    if (ms/10 < 10) printNum(0);
    printNum(ms/10); printString("s");
}
void Print::printTime(int milliseconds)
{
    int d=0, h=0, m=0, s=0, ms=0;

    Time::getTotalTime(milliseconds, &d,&h,&m,&s,&ms);

    if (d>0) {printNum(d); printString("d ");}
    if (h>0 || d>0) {printNum(h); printString("h ");}
    if (m>0 || h>0 || d>0) {printNum(m); printString("m ");}
    printNum(s); printString(".");
    if (ms/10 < 10) printNum(0);
    printNum(ms/10); printString("s");
}

void Print::printString(std::string str)
{
    for (unsigned int i = 0; i < str.length(); i++)
    {
        if (str.at(i) == '\n') {Print::newLine();}
        else glutBitmapCharacter(font, str.at(i));
    }
}

void Print::printScramble()
{
    if (needNewScramble)
    {
        Print::deleteScramble();
        int numMoves = 20;

        int oldRand1 = -1;
        int randNum1 = -1;
        int randNum2 = -1;
        for (int i = 0; i <= numMoves; i++)
        {
            for (; randNum1 == oldRand1; randNum1 = rand() % 6);

            switch (randNum1)
            {
                case 0: pattern += "D"; break; case 1: pattern += "L"; break;
                case 2: pattern += "R"; break; case 3: pattern += "U"; break;
                case 4: pattern += "B"; break; case 5: pattern += "F"; break;
            }
            randNum2 = rand() % 3;
            switch (randNum2)
            {
                case 0: pattern += "\'"; break;
                case 1: case 2: pattern += ""; break;
            }
            if (i!=numMoves) pattern += " ";

            oldRand1 = randNum1;
        }
        needNewScramble = false;
    }
    Print::printString(pattern);
}
void Print::needScramble()
{
    needNewScramble = true;
}
void Print::deleteScramble()
{
    pattern = "";
}

void Print::newLine()
{
    if (font == smFont) yRaster -= .085;
    //else if (font == medFont) yRaster -= .11;
    else yRaster -= .125;
    glRasterPos2f(xRaster,yRaster);
}




