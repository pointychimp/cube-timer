#include "../inc/log.h"

static char temp[] = "temp.txt";
//static char temp[] = "D:\\Games_And_Programming\\CodeBlocks Builds\\Cube Timer\\temp.txt";
static std::fstream logFile;

void Log::write(std::string fileName, unsigned int time)
{
    std::ofstream tempFile;
    tempFile.open(temp);
    if (tempFile.is_open())
    {
        char Time[20];
        Log::getDateTime(Time);
        tempFile << Time << " " << time << std::endl;

        logFile.open(fileName.c_str());
        if (logIsOpen())
        {
            std::string dateTime;
            int data;
            std::vector<std::string> dates;
            std::vector<int> times;

            while (!logIsEOF())
            {
                logFile >> dateTime >> data;
                dates.push_back(dateTime); times.push_back(data);
            }
            dates.pop_back(); times.pop_back();
            for (unsigned int i = 0; i < dates.size() && dates.size() == times.size(); i++)
            {
                tempFile << dates.at(i) << " " << times.at(i) << std::endl;
            }
            closeLog();
        }
        else std::cout << "couldn't open log file to read/write.\n";

        tempFile.close();
    }
    else std::cout << "Couldn't open temp file to write.\n";

    remove(fileName.c_str());
    rename(temp, fileName.c_str());
    remove(temp);
}
unsigned int Log::readInt()
{
    unsigned int i;
    logFile >> i;
    return i;
}
std::string Log::readString()
{
    std::string str;
    logFile >> str;
    return str;
}
void Log::openLog(std::string fileName)
{
    logFile.open(fileName.c_str());
    if (!logFile.is_open()) std::cout << "Couldn't open log file to read.\n";
}
bool Log::logIsOpen()
{
    return logFile.is_open();
}
bool Log::logIsEOF()
{
    return logFile.eof();
}
void Log::closeLog()
{
    logFile.close();
}


void Log::getDateTime(char* t)
{
    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);
    strftime (t,20,"[%x_%X]",timeinfo);

}
